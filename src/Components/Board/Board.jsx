import React, { Component } from 'react';
import { Card, Button, FormControl } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import List from './List/List';
import CardModal from './CardModal/CardModal';
import './Board.css';

class Board extends Component {
  state = {
    lists: [],
    show: false,
    card: {},
    newListName:''
  };
  APIKey = '92afb903ae91d350b03bfc14ccb1140f';
  APIToken = '649e5f75631c9978b1272f8ff83939fad7063a68bcc98d1a7fb7a792c7c2cd8d';

  componentDidMount() {
    fetch(
      `https://api.trello.com/1/boards/${this.props.match.params.idBoard}?fields=all&key=${this.APIKey}&token=${this.APIToken}`
    ).then(response => {
      response.json().then(board => {
        this.setState({ board: board });
      });
    });
    fetch(
      `https://api.trello.com/1/boards/${this.props.match.params.idBoard}/lists?cards=none&fields=all&key=${this.APIKey}&token=${this.APIToken}`
    ).then(response => {
      response.json().then(lists => {
        this.setState({ lists: lists });
      });
    });
  }

  handleClose = () => {
    this.setState({ show: false });
  };
  handleShow = card => {
    this.setState({
      show: true,
      card: card
    });
  };

  handleInputValue = event => {
    const value=event.target.value;
    this.setState({newListName:value});
  }

  addList=()=>{
    if(this.state.newListName.trim()==='')
    {
      alert('Please type a new List Name...');
      return(0);
    }
    fetch(`https://api.trello.com/1/lists?name=${this.state.newListName}&pos=bottom&idBoard=${this.state.board.id}&key=${this.APIKey}&token=${this.APIToken}`,{method:'POST'}).then(response=>{
      response.json().then(newList=>{
        this.setState({
          lists:this.state.lists.concat(newList),
          newListName:''
      });
      })
    })
  }

  deleteList=(list)=>{
    fetch(`https://api.trello.com/1/lists/${list.id}/closed?value=true&key=${this.APIKey}&token=${this.APIToken}`,{method:'PUT'}).then(response=>{
      response.json().then(()=>{
        this.setState({lists:this.state.lists.filter(listData=>listData!==list)});
      })
    })
  }
  checklists
  render() {
    const style = {
      backgroundImage:
        this.state.board &&
        `url(${this.state.board&&this.state.board.prefs&&this.state.board.prefs.backgroundImageScaled&&this.state.board.prefs.backgroundImageScaled[7].url})`,
        backgroundColor:this.state.board&&this.state.board.prefs&&this.state.board.prefs.background,
      backgroundSize: '100%'
    };

    return (
      <React.Fragment>
        <div className='cardPad'>
          <Card className='text-center'>
            <Card.Header>
              <Link to="/">Board: </Link>
              {this.state.board && this.state.board.name}
            </Card.Header>
            <Card.Body style={style}>
              <div className='list-box'>
                {this.state.lists.map(list => {
                  return (
                    <div key={list.id} className="list-holder">
                    <List
                      APIKey={this.APIKey}
                      APIToken={this.APIToken}
                      list={list}
                      handleShow={this.handleShow}
                      deleteList={this.deleteList}
                    />
                    </div>
                  );
                })}
              </div>
            </Card.Body>
            <Card.Footer className='text-muted'>
              <FormControl placeholder="New List..." value={this.state.newListName} onChange={this.handleInputValue} />
              <Button variant='primary' onClick={this.addList} >Add List +</Button>
            </Card.Footer>
          </Card>
        </div>
        <CardModal
          show={this.state.show}
          card={this.state.card}
          handleClose={this.handleClose}
          APIKey={this.APIKey}
          APIToken={this.APIToken}
        />
      </React.Fragment>
    );
  }
}

export default Board;