import React from 'react';
import { ListGroup, Button } from 'react-bootstrap';
import './CheckItem.css';

const CheckItem = props => {
  return (
    <React.Fragment>
      <ListGroup.Item>
        <div className='checkitem-holder'>
          <label
          style={props.checkItem.state === 'complete' ?{textDecoration:'line-through',color:'rgba(200, 200, 200, 1)'}:{textDecoration:'none',color:'rgba(100, 100, 100, 1)'}}
            className='check-item'
            onClick={event => {
              event.preventDefault();
              props.updateChecked(props.checkItem);
            }}
          >
            {props.checkItem.name}
            <input
              type='checkbox'
              checked={props.checkItem.state === 'complete' ? true : false}
              readOnly
            />
            <span className='checkmark' />{' '}
          </label>
          <Button variant='danger' onClick={()=>{props.deleteCheckItem(props.checkItem)}}>
            &#215;
          </Button>
        </div>
      </ListGroup.Item>
    </React.Fragment>
  );
};

export default CheckItem;
