import React, { Component } from 'react';
import { Card, ListGroup, Button, FormControl } from 'react-bootstrap';
import CheckItem from './CheckItem/CheckItem';
import'./Checklist.css';

class Checklist extends Component {
  state = {
      checkItems:[],
      newCheckItemName:''
  };

  componentDidMount(){
      fetch(`https://api.trello.com/1/checklists/${this.props.checklist.id}/checkItems?key=${this.props.APIKey}&token=${this.props.APIToken}`).then(response=>{
          response.json().then(checkItems=>{
              this.setState({
                  checkItems:checkItems,
              })
          })
      })
  }

  updateChecked = (checkItem) => {
      const newState = (checkItem.state==='complete')? 'incomplete': 'complete' ;
      fetch(`https://api.trello.com/1/cards/${this.props.idCard}/checkItem/${checkItem.id}?state=${newState}&key=${this.props.APIKey}&token=${this.props.APIToken}`,{method:'PUT'}).then(response=>{
          response.json().then(updateData=>{
              let tempCheckItems = this.state.checkItems;
              tempCheckItems[tempCheckItems.indexOf(checkItem)].state=updateData.state;
              this.setState({checkItems:tempCheckItems});
          })
      })
  }

  deleteCheckItem=(checkItem)=>{
    fetch(`https://api.trello.com/1/checklists/${this.props.checklist.id}/checkItems/${checkItem.id}?key=${this.props.APIKey}&token=${this.props.APIToken}`,{method:'DELETE'}).then(response=>{
      response.json().then(()=>{
        this.setState({checkItems:this.state.checkItems.filter(checkItemData=>checkItemData!==checkItem)});
      })
    })
  }

  handleInputValue = event => {
    const value = event.target.value;
    this.setState({ newCheckItemName: value });
  }

  addCheckItem=()=>{
      if(this.state.newCheckItemName.trim()==='')
      {
          alert('Please type a new CheckItem name...');
          return(0);
      }
      fetch(`https://api.trello.com/1/checklists/${this.props.checklist.id}/checkItems?name=${this.state.newCheckItemName}&pos=bottom&checked=false&key=${this.props.APIKey}&token=${this.props.APIToken}`,{method:'POST'}).then(response=>{
          response.json().then(newCheckItem=>{
              this.setState({checkItems:this.state.checkItems.concat([newCheckItem]),
                newCheckItemName:''
              });
          })
      })
  }

  render() {
      const percentComplete = this.state.checkItems.length>0? (100*this.state.checkItems.filter(checkItem=>checkItem.state==='complete').length/this.state.checkItems.length).toFixed(2):0;
    return (
      <Card>
        <Card.Header>
        <div className='checklist-header'>
              <Card.Title>{this.props.checklist.name}</Card.Title>
              <Button variant='danger' onClick={()=>{this.props.deleteChecklist(this.props.checklist)}}>
                Delete
              </Button>
            </div>
            Percent Complete: {percentComplete+'%'}
            <div className="status-bar">
            <div style={{width:`${percentComplete}%`}}></div>
            </div>
        </Card.Header>
        <Card.Body>
        <ListGroup variant='flush'>
          {this.state.checkItems.map(checkItem=><CheckItem key={checkItem.id} checkItem={checkItem} updateChecked={this.updateChecked} deleteCheckItem={this.deleteCheckItem} />)}
        </ListGroup>
        </Card.Body>
        <Card.Footer className='text-muted'>
            <FormControl
              placeholder='New CheckItem...'
              value={this.state.newCheckItemName}
              onChange={this.handleInputValue}
            />
            <Button variant='primary' onClick={this.addCheckItem}>
              Add CheckItem +
            </Button>
          </Card.Footer>

      </Card>
    );
  }
}

export default Checklist;
