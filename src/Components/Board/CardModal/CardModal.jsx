import React, { Component } from 'react';
import { Modal, Button, FormControl } from 'react-bootstrap';
import Checklist from './Checklist/Checklist';
import './CardModal.css';

class CardModal extends Component {
  state = {
    checklists: [],
    newChecklistName:'',
  };

  componentDidUpdate(prevProps) {
    if (this.props.card.id !== prevProps.card.id) {
      if (this.state.checklists.length !== 0) {
        this.setState({ checklists: [] });
      }
      fetch(
        `https://api.trello.com/1/cards/${this.props.card.id}/checklists?&key=${this.props.APIKey}&token=${this.props.APIToken}`
      ).then(response => {
        response.json().then(checklists => {
          this.setState({
            checklists: checklists,
          });
        });
      });
    }
  }

  handleInputValue = event => {
    const value = event.target.value;
    this.setState({ newChecklistName: value });
  }

  addChecklist=()=>{
    if(this.state.newChecklistName.trim()===''){
      alert('Please type a new Checklist name...')
    }
    fetch(`https://api.trello.com/1/checklists?name=${this.state.newChecklistName}&idCard=${this.props.card.id}&key=${this.props.APIKey}&token=${this.props.APIToken}`,{method:'POST'}).then(response=>{
      response.json().then(newChecklist=>{
        this.setState({
          checklists:this.state.checklists.concat([newChecklist]),
          newChecklistName:''
        });
      })
    })
  }

  deleteChecklist=(checklist)=>{
    fetch(`https://api.trello.com/1/checklists/${checklist.id}?key=${this.props.APIKey}&token=${this.props.APIToken}`,{method:'DELETE'}).then(response=>{
      response.json().then(()=>{
        this.setState({checklists:this.state.checklists.filter(checklistData=>checklistData!==checklist)});
      })
    })
  }

  render() {
    return (
      <Modal show={this.props.show} onHide={this.props.handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{this.props.card.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {this.state.checklists.map(checklist => (
            <Checklist
              key={checklist.id}
              checklist={checklist}
              idCard={this.props.card.id}
              APIKey={this.props.APIKey}
              APIToken={this.props.APIToken}
              deleteChecklist={this.deleteChecklist}
            />
          ))}
        </Modal.Body>
        <Modal.Footer>
          <FormControl
            placeholder='New Checklist...'
            value={this.state.newChecklistName}
            onChange={this.handleInputValue}
          />
          <Button variant='primary' onClick={this.addChecklist}>Add Checklist</Button>
          <Button variant='secondary' onClick={this.props.handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default CardModal;
