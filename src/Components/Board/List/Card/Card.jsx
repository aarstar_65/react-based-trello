import React from 'react';
import { ListGroupItem, Button } from 'react-bootstrap';
import './Card.css';

const Card = props => {
  return (
    <React.Fragment>
      <ListGroupItem>
      <div className="card-holder">
        <span className='card-class'
          onClick={() => {
            props.handleShow(props.card);
          }}
        >
          {props.card.name}
        </span>
        <Button variant='danger' onClick={()=>{props.deleteCard(props.card)}}>
          &#215;
        </Button>
        </div>
      </ListGroupItem>
    </React.Fragment>
  );
};

export default Card;
