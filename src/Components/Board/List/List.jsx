import React, { Component } from 'react';
import { Card, Button, ListGroup, FormControl } from 'react-bootstrap';
import CardElement from './Card/Card';
import './List.css';

class List extends Component {
  state = {
    cards: [],
    newCardName: ''
  };

  componentDidMount() {
    fetch(
      `https://api.trello.com/1/lists/${this.props.list.id}/cards?fields=name,url&key=${this.props.APIKey}&token=${this.props.APIToken}`
    ).then(response => {
      response.json().then(cards => {
        this.setState({ cards: cards });
      });
    });
  }

  handleInputValue = event => {
    const value = event.target.value;
    this.setState({ newCardName: value });
  };

  addCard = () => {
    if (this.state.newCardName.trim() === '') {
      alert('Please type a new Card Name...');
      return 0;
    }
    fetch(
      `https://api.trello.com/1/cards?idList=${this.props.list.id}&name=${this.state.newCardName}&key=${this.props.APIKey}&token=${this.props.APIToken}`,
      { method: 'POST' }
    ).then(response => {
      response.json().then(newCard => {
        this.setState({
          cards: this.state.cards.concat(newCard),
          newCardName: ''
        });
      });
    });
  };
  
  deleteCard=(card)=>{
    fetch(`https://api.trello.com/1/cards/${card.id}?key=${this.props.APIKey}&token=${this.props.APIToken}`,{method:'DELETE'}).then(response=>{
      response.json().then(()=>{
        this.setState({cards:this.state.cards.filter(cardData=>cardData!==card)});
      })
    })
  }

  render() {
    return (
      <React.Fragment>
        <Card>
          <Card.Header>
            <div className='list-header'>
              <Card.Title>{this.props.list.name}</Card.Title>
              <Button variant='danger' onClick={()=>{this.props.deleteList(this.props.list)}}>
                Delete
              </Button>
            </div>
          </Card.Header>
          <Card.Body>
            <ListGroup className='list-group-flush'>
              {this.state.cards.map(card => {
                return (
                  <CardElement
                    key={card.id}
                    card={card}
                    handleShow={this.props.handleShow}
                    deleteCard={this.deleteCard}
                  />
                );
              })}
            </ListGroup>
          </Card.Body>
          <Card.Footer className='text-muted'>
            <FormControl
              placeholder='New Card...'
              value={this.state.newCardName}
              onChange={this.handleInputValue}
            />
            <Button variant='primary' onClick={this.addCard}>
              Add Card +
            </Button>
          </Card.Footer>
        </Card>
      </React.Fragment>
    );
  }
}

export default List;
