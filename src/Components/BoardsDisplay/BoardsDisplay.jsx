import React, { Component } from 'react';
import BoardCard from './BoardCard/BoardCard';
import './BoardsDisplay.css';

class BoardsDisplay extends Component {
  state = {
    boards: []
  };
  componentDidMount() {
    fetch(
      `https://api.trello.com/1/members/me/boards?fields=all&key=${this.props.APIKey}&token=${this.props.APIToken}`
    ).then(response => {
      response.json().then(boards => {
        this.setState({ boards: boards });
      });
    });
    console.log(this.state);
  }

  render() {
    return (
      <React.Fragment>
        <div className='board-nav'>
          {this.state.boards.map(board => {
            return (
              <BoardCard
                key={board.id}
                board={board}
                routeToBoard={this.props.routeToBoard}
              />
            );
          })}
        </div>
      </React.Fragment>
    );
  }
}

export default BoardsDisplay;
