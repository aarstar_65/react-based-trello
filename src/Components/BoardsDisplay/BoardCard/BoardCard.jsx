import React from 'react';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './BoardCard.css';

const BoardCard = (props) => {
    return (
        <React.Fragment>
          <Card style={{ width: '18rem' }}>
            <Card.Img
              variant='top'
              src={props.board&&props.board.prefs&&props.board.prefs.backgroundImageScaled&&props.board.prefs.backgroundImageScaled[0].url}
              style={{backgroundColor:props.board&&props.board.prefs&&props.board.prefs.background}}/>
            <Card.Body>
              <Card.Title>{props.board.name}</Card.Title>
              <Link to={"/board/"+props.board.id} onClick={()=>{props.routeToBoard(props.board)}}>Go to Board &rarr;</Link>
            </Card.Body>
          </Card>
        </React.Fragment>
    );
}
 
export default BoardCard;
