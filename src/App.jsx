import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import BoardsDisplay from './Components/BoardsDisplay/BoardsDisplay'
import Board from './Components/Board/Board';
import './App.css'

class App extends Component {
  state = { 
    board:{}
   }
  APIKey = '92afb903ae91d350b03bfc14ccb1140f';
  APIToken = '649e5f75631c9978b1272f8ff83939fad7063a68bcc98d1a7fb7a792c7c2cd8d';
  routeToBoard =(board)=>{
    this.setState({board:board});
  }
  render() { 
    return ( 
      <BrowserRouter>
        <header>
          <span className="logo"></span>
        </header>
        <Switch>
          <Route exact path="/">
            <BoardsDisplay APIKey={this.APIKey} APIToken={this.APIToken} routeToBoard={this.routeToBoard} />
          </Route>
          <Route exact path="/board/:idBoard" component={Board} />
        </Switch>
      </BrowserRouter>
    );
  }
}
 
export default App;
